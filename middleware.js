const jwt = require('jsonwebtoken');
require('dotenv').config();

const auth = (req, res, next) => {
    var token;
    if (req.headers.authorization != undefined) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            token = req.headers.authorization.split(' ')[1];
        } else {
            token = req.headers.authorization;
        }

        if (token.length > 0) {
            jwt.verify(token, process.env.KEY_SECRET, (err, decoded) => {
                if (err) {
                    res.status(401).send({
                        status: 401,
                        success: false,
                        message: 'Unauthorized'
                    });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            res.status(401).send({
                status: 401,
                success: false,
                message: 'Unauthorized'
            });
        }
    } else {
        res.status(401).send({
            status: 401,
            success: false,
            message: 'Unauthorized'
        });
    }
}




module.exports = {
    auth: auth
}