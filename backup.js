const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const validator = require('validator');
const jwt = require("jwt-simple");
const ExtractJwt = require("passport-jwt").ExtractJwt;
const JwtStrategy = require("passport-jwt").Strategy;
const passport = require("passport");

require('dotenv').config();

const SECRET = process.env.KEY_SECRET;
var uuid;


const db = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE
});

db.connect();


const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const auth = (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    let sql = 'SELECT id FROM admin WHERE username = "'+username+'" AND password = SHA1("'+password+'")';
    let query = db.query(sql, (err, result) => {
        if(result.length > 0) {
            user = req.body.username;
            db.query('UPDATE admin SET timestamp = CURRENT_TIMESTAMP WHERE username = "'+username+'"');
            const dataUUID = JSON.stringify(result)
            const stringify = JSON.parse(dataUUID);
            for (var i = 0; i < stringify.length; i++) {
                uuid = stringify[i]['id'];
            }
            next();
        } else {
            res.sendStatus(401);
        }
    })
}

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader("authorization"),
    secretOrKey: SECRET,
 }
 const jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
    if(payload.sub) done(null, true);
    else done(null, false);
 });

passport.use(jwtAuth);

const requireJWTAuth = passport.authenticate("jwt",{session:false});


app.get("/", requireJWTAuth, (req, res) => {
    
    res.send("ยอดเงินคงเหลือ 50");
 });


app.get('/data', (req,res) => {
    let sql = 'SELECT * FROM items';
    let query = db.query(sql, (err, result) => {
        if (err) throw err;
        res.end(JSON.stringify(result));
    })
});

app.get('/data/brand', (req,res) => {
    let sql = 'SELECT brand FROM items';
    let query = db.query(sql, (err, result) => {
        if (err) throw err;
        res.end(JSON.stringify(result));
    })
});

app.get('/data/brand/:brand', (req,res) => {
    let sql = 'SELECT * FROM items WHERE brand ="' + req.params.brand + '"';
    let query = db.query(sql, (err, result) => {
        if (err) throw err;
        res.end(JSON.stringify(result));
    })
});

app.post('/login', auth, (req,res) => {
    console.log(uuid)
    const payload = {
        sub: uuid,
        iat: new Date().getTime()
     };
     res.send(jwt.encode(payload, SECRET, 'HS512'));
});


app.get('*', (req, res) => {
    res.sendStatus(404);
  });



app.listen('3000',() => {     // 
    console.log('start port 3000')  
    });
