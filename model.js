var JWT = require('jsonwebtoken');
var mysql = require('mysql')
var model = module.exports;
const bcrypt = require('bcrypt');
require('dotenv').config();

const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DATABASE
});

db.connect();

// based on https://github.com/thomseddon/node-oauth2-server/tree/master/examples/memory

var JWT_ISSUER = 'application';
var JWT_SECRET = process.env.KEY_SECRET;

// the expiry times should be consistent between the oauth2-server settings
// and the JWT settings (not essential, but makes sense)
model.JWT_ACCESS_TOKEN_EXPIRY_SECONDS = 15;             // 30 minutes
model.JWT_REFRESH_TOKEN_EXPIRY_SECONDS = 1209600;         // 14 days

// In-memory datastores
var oauthClients = [{
  clientId: 'admin',
  clientSecret: 'mayawizard',
  redirectUri: ''
}];

// key is grant_type
// value is the array of authorized clientId's
var authorizedClientIds = {
  password: [
    'admin'
  ],
  refresh_token: [
    'admin'
  ]
};

// current registered users
let sql = 'SELECT id, username, password FROM admin';
let users = []
var query = db.query(sql, (err, result) => {
  if (err) {
    res.status(500).json({
      message: {
        errno: err.errno,
        errdecode: err.code,
        data: err
      }
    });
  } else {
    for (i = 0; i < result.length; i++) {
      let schema = {
        id : result[i].id,
        clientId : 'admin',
        username : result[i].username,
        password : result[i].password
      }

      users.push(schema)
    }
  }
})



// Functions required to implement the model for oauth2-server

// generateToken
// This generateToken implementation generates a token with JWT.
// the token output is the Base64 encoded string.
model.generateToken = function (type, req, callback) {
  var token;
  var secret;
  var user = req.user;
  var exp = new Date();
  var payload = {
    // public claims
    iss: JWT_ISSUER,   // issuer
    //    exp: exp,        // the expiry date is set below - expiry depends on type
    //    jti: '',         // unique id for this token - needed if we keep an store of issued tokens?
    // private claims
    userId: user.id,
    username: user.username
  };
  var options = {
    algorithms: ['HS256']  // HMAC using SHA-256 hash algorithm
  };

  if (type === 'accessToken') {
    secret = JWT_SECRET;
    exp.setSeconds(exp.getSeconds() + model.JWT_ACCESS_TOKEN_EXPIRY_SECONDS);
  } else {
    secret = JWT_SECRET;
    exp.setSeconds(exp.getSeconds() + model.JWT_REFRESH_TOKEN_EXPIRY_SECONDS);
  }
  payload.exp = exp.getTime();

  token = JWT.sign(payload, secret, options);

  callback(false, token);
};

// The bearer token is a JWT, so we decrypt and verify it. We get a reference to the
// user in this function which oauth2-server puts into the req object
model.getAccessToken = function (bearerToken, callback) {
  //console.log('bearerToken : ' + bearerToken);
  return JWT.verify(bearerToken, JWT_SECRET, function (err, decoded) {
    //console.log(decoded);

    if (err) {
      return callback(err, false);   // the err contains JWT error data
    }

    // other verifications could be performed here
    // eg. that the jti is valid

    // we could pass the payload straight out we use an object with the
    // mandatory keys expected by oauth2-server, plus any other private
    // claims that are useful
    return callback(false, {
      expires: new Date(decoded.exp),
      user: getUserById(decoded.userId)
    });
  });
};


// As we're using JWT there's no need to store the token after it's generated
model.saveAccessToken = function (accessToken, clientId, expires, userId, callback) {
  return callback(false);
};

// The bearer token is a JWT, so we decrypt and verify it. We get a reference to the
// user in this function which oauth2-server puts into the req object
model.getRefreshToken = function (bearerToken, callback) {
  //console.log('refresh bearerToken : ' + bearerToken);
  return JWT.verify(bearerToken, JWT_SECRET, function (err, decoded) {
    //console.log(decoded);
    //console.log(err);
    if (err) {
      return callback(err, false);
    }

    // other verifications could be performed here
    // eg. that the jti is valid

    // instead of passing the payload straight out we use an object with the
    // mandatory keys expected by oauth2-server plus any other private
    // claims that are useful
    let user_info = getUserById(decoded.userId);
    return callback(false, {
      expires: new Date(decoded.exp),
      user: user_info,
      clientId: user_info.clientId
    });
  });
};

// required for grant_type=refresh_token
// As we're using JWT there's no need to store the token after it's generated
model.saveRefreshToken = function (refreshToken, clientId, expires, userId, callback) {
  return callback(false);
};

// authenticate the client specified by id and secret
model.getClient = function (clientId, clientSecret, callback) {
  for (var i = 0, len = oauthClients.length; i < len; i++) {
    var elem = oauthClients[i];
    if (elem.clientId === clientId &&
      (clientSecret === null || elem.clientSecret === clientSecret)) {
      return callback(false, elem);
    }
  }
  callback(false, false);
};

// determine whether the client is allowed the requested grant type
model.grantTypeAllowed = function (clientId, grantType, callback) {
  callback(false, authorizedClientIds[grantType] &&
    authorizedClientIds[grantType].indexOf(clientId.toLowerCase()) >= 0);
};

// authenticate a user
// for grant_type password
model.getUser = function (username, password, callback) {
  for (var i = 0, len = users.length; i < len; i++) {
    var elem = users[i];
    if (elem.username === username && bcrypt.compareSync(password, elem.password)) {
      return callback(false, elem);
    }
  }
  callback(false, false);
};

var getUserById = function (userId) {
  for (var i = 0, len = users.length; i < len; i++) {
    var elem = users[i];
    if (elem.id === userId) {
      return elem;
    }
  }
  return null;
};

// for grant_type client_credentials
// given client credentials
//   authenticate client
//   lookup user
//   return that user...
//     oauth replies with access token and renewal token
//model.getUserFromClient = function(clientId, clientSecret, callback) {
//
//};