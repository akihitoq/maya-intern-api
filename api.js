const mysql = require('mysql');
const bcrypt = require('bcrypt');
const validator = require('validator');

require('dotenv').config();

const saltRounds = 10;

const db = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE
});

db.connect();

class Api {
    addItem(req, res) {
        let urlImage = req.body.image;
        let name = req.body.name
        let brand = req.body.brand
        let type = req.body.type
        let os = req.body.os
        let owner = req.body.owner
        let status = 'Available';
        let admin_id = req.body.admin_id
        let del = 0

        if (name && brand && type && os && owner && admin_id && urlImage) {

            let sql = 'INSERT INTO items (name, brand, type, image, os, status, del, owner, adminAddID) VALUES '
            sql += '("' + name + '","' + brand + '","' + type + '","' + urlImage + '","' + os + '","' + status + '","' + del + '","' + owner + '","' + admin_id + '")'
            let query = db.query(sql, (err, result) => {
                if (err) {
                    res.status(500).json({
                        message: {
                            errno: err.errno,
                            errdecode: err.code,
                            data: err
                        }
                    });
                }
                else {
                    if (result.length < 1) {
                        res.status(400).send({
                            message: 'Bad request',
                        });
                    }
                    else {
                        let sql = 'SELECT a.id, a.name AS name, b.name as brand, c.name as type, a.image, d.name as os, e.name as owner, a.status ';
                        sql += 'FROM items as a, brand as b, type as c, os as d, owner as e ';
                        sql += 'WHERE a.id = ' + result.insertId + ' AND a.del != 1 AND a.brand = b.id AND a.type = c.id AND a.os = d.id AND a.owner = e.id'
                        db.query(sql, (errs, results) => {
                            if (err) {
                                res.status(500).json({
                                    message: {
                                        errno: err.errno,
                                        errdecode: err.code,
                                        data: errs
                                    }
                                });
                            } else {
                                sql = 'INSERT INTO history (id_item, id_employee, admin_approve, timestamp, status, info) '
                                sql += 'VALUES (' + results[0].id + ',0, ' + admin_id + ', CURRENT_TIMESTAMP, "Edit", "")'
                                db.query(sql, (error, history) => {
                                    if (error) {
                                        res.status(500).json({
                                            message: {
                                                errno: err.errno,
                                                errdecode: err.code,
                                                data: err
                                            }
                                        });
                                    } else {
                                        res.status(201).json({
                                            message: 'CREATED',
                                            detial: results[0]
                                        });
                                    }
                                })
                            }
                        })
                    }
                }
            });
        } else {
            res.status(400).send({
                message: 'Bad request',
            });
        }
    }

    listItems(req, res) {
        let sql = 'SELECT a.id, a.name AS name, b.name as brand, c.name as type, a.image, d.name as os, e.name as owner, a.status ';
        sql += 'FROM items as a, brand as b, type as c, os as d, owner as e ';
        sql += 'WHERE a.del != 1 AND a.brand = b.id AND a.type = c.id AND a.os = d.id AND a.owner = e.id'
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                if (result.length > 0) {
                    res.status(200).send({
                        data: {
                            items: result
                        }
                    });
                } else {
                    res.status(404).send({
                        message: 'Not Found'
                    })
                }
            }
        })
    }

    getItemDetial(req, res) {
        let item = req.params.item;
        let sql = 'SELECT a.id, a.name AS name, b.name as brand, c.name as type, a.image, d.name as os, e.name as owner, a.status ';
        sql += 'FROM items as a, brand as b, type as c, os as d, owner as e '
        sql += 'WHERE a.id = "' + item + '" AND a.brand = b.id AND a.type = c.id AND a.os = d.id AND a.owner = e.id AND a.del != 1';
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                if (result.length < 1) {
                    res.status(404).send({
                        message: 'Not found',
                    });
                }
                else {
                    res.status(200).send({
                        message: 'OK',
                        result: result
                    });
                }
            }
        })
    }


    filterPlatform(req, res) {
        let platform = req.query.platform
        // let status = req.query.status
        let sql;
        // let sql = 'SELECT * FROM items WHERE os = "' + platform + '" AND del != 1';
        if (platform) {
            sql = 'SELECT a.id, a.name AS name, b.name as brand, c.name as type, a.image, d.name as os, e.name as owner, a.status '
            sql += 'FROM items as a, brand as b, type as c, os as d, owner as e '
            sql += 'WHERE d.name = "' + platform + '" AND a.del != 1 AND a.brand = b.id AND a.type = c.id AND a.os = d.id AND a.owner = e.id'
        } else {
            sql = 'SELECT a.id, a.name AS name, b.name as brand, c.name as type, a.image, d.name as os, e.name as owner, a.status ';
            sql += 'FROM items as a, brand as b, type as c, os as d, owner as e ';
            sql += 'WHERE a.del != 1 AND a.brand = b.id AND a.type = c.id AND a.os = d.id AND a.owner = e.id'
        }
        // if (platform && status) {
        //     sql = 'SELECT * FROM items WHERE ( os = "' + platform + '" AND status = "' + status + '") AND del != 1'
        // } else if (platform && !status) {
        //     sql = 'SELECT * FROM items WHERE os = "' + platform + '" AND del != 1'
        // } else if (!platform && status) {
        //     sql = 'SELECT * FROM items WHERE status = "' + status + '" AND del != 1'
        // } else {
        //     sql = 'SELECT * FROM items WHERE ( os = "' + platform + '" AND status = "' + status + '") AND del != 1'
        // }
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                if (result.length < 1) {
                    res.status(404).send({
                        message: 'Not found',
                    });
                }
                else {
                    res.status(200).send({
                        message: 'OK',
                        result: result
                    });
                }
            }
        })
    }

    editItem(req, res) {
        let id = req.params.id
        let admin = req.body.admin_id
        if (validator.isNumeric(id) && admin) {
            let name = req.body.name
            let brand = req.body.brand
            let type = req.body.type
            let os = req.body.os
            let owner = req.body.owner
            let sql = 'UPDATE items SET name = "' + name + '", brand = "' + brand + '", type ="' + type + '", os = "' + os + '", owner = "' + owner + '" WHERE id =' + id + ''
            let query = db.query(sql, (err, result) => {
                if (err) {
                    res.status(500).json({
                        message: {
                            errno: err.errno,
                            errdecode: err.code,
                            data: err
                        }
                    });
                }
                else {
                    if (result.length < 1) {
                        res.status(400).send({
                            message: 'Bad request',
                        });
                    }
                    else {
                        let sql = 'SELECT a.id, a.name AS name, b.name as brand, c.name as type, a.image, d.name as os, e.name as owner, a.status ';
                        sql += 'FROM items as a, brand as b, type as c, os as d, owner as e '
                        sql += 'WHERE a.id = "' + id + '" AND a.brand = b.id AND a.type = c.id AND a.os = d.id AND a.owner = e.id';
                        db.query(sql, (errs, results) => {
                            if (err) {
                                res.status(500).json({
                                    message: {
                                        errno: err.errno,
                                        errdecode: err.code,
                                        data: err
                                    }
                                });
                            } else {
                                sql = 'INSERT INTO history (id_item, id_employee, admin_approve, timestamp, status, info) '
                                sql += 'VALUES (' + id + ',0, ' + admin + ', CURRENT_TIMESTAMP, "Edit", "")'
                                db.query(sql, (error, history) => {
                                    if (error) {
                                        res.status(500).json({
                                            message: {
                                                errno: err.errno,
                                                errdecode: err.code,
                                                data: err
                                            }
                                        });
                                    } else {
                                        res.status(200).json({
                                            message: 'OK',
                                            detial: results[0]
                                        });
                                    }
                                })
                            }
                        })
                    }
                }
            });
        } else {
            res.status(400).send({
                message: 'Bad request',
            });
        }

    }

    deleteItem(req, res) {
        let itemsID = req.params.itemID;
        let admin = req.body.admin_id
        if (validator.isNumeric(itemsID) && admin) {
            let admin_id = req.body.admin_id
            let sql = 'UPDATE items SET del = 1 , adminDeleteID = "' + admin_id + '" WHERE id = "' + itemsID + '"'
            let query = db.query(sql, (err, result) => {
                if (err) throw err;
                else {
                    if (result.length < 1) {
                        res.status(400).send({
                            message: 'Bad request',
                        });
                    }
                    else {
                        sql = 'INSERT INTO history (id_item, id_employee, admin_approve, timestamp, status, info) '
                        sql += 'VALUES (' + itemsID + ',0, ' + admin + ', CURRENT_TIMESTAMP, "Delete", "")'
                        db.query(sql, (error, history) => {
                            if (error) {
                                res.status(500).json({
                                    message: {
                                        errno: err.errno,
                                        errdecode: err.code,
                                        data: err
                                    }
                                });
                            } else {
                                res.status(204).json({
                                    message: 'OK',
                                });
                            }
                        })
                    }
                }
            });
        } else {
            res.status(400).send({
                message: 'Bad request',
            });
        }
    }

    addAdmin(req, res) {
        let username = req.body.username
        let password = req.body.password
        let name = req.body.name
        let profile = req.body.picture
        let nickname = req.body.nickname
        let email = req.body.email
        let telephone = req.body.telephone
        let checkUsername = 'SELECT username FROM admin WHERE username = "' + username + '"'
        if (username && password && name) {
            db.query(checkUsername, (errs, check) => {
                if (check.length < 1) {
                    bcrypt.hash(password, saltRounds).then(function (hash) {
                        let sql = 'INSERT INTO admin (name, username, password, profile, nickname, email, telephone)';
                        sql += 'VALUES ("' + name + '","' + username + '","' + hash + '","' + profile + '","' + nickname + '","' + email + '","' + telephone + '")'
                        let query = db.query(sql, (err, result) => {
                            if (err) throw err;
                            else {
                                res.status(200).send({
                                    message: 'OK',
                                });
                            }
                        });
                    });
                } else {
                    res.status(400).send({
                        message: 'Username Unavailable'
                    });
                }
            });
        } else {
            res.status(400).send({
                message: 'Bad Request',
            });
        }

    }

    addEmployee(req, res) {
        let name = req.body.name
        let profile = req.body.picture
        let nickname = req.body.nickname
        let email = req.body.email
        let telephone = req.body.telephone
        let department = req.body.department

        let sql = 'INSERT INTO employee ( name, profile, nickname, email, telephone, depart)';
        sql += 'VALUES ("' + name + '","' + profile + '","' + nickname + '","' + email + '","' + telephone + '","' + department + '")"'
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                res.status(200).send({
                    message: 'OK',
                });
            }
        });
    }

    borrows(req, res) {
        let itemsID = req.params.id;
        let employee = req.body.employee_id;
        let admin = req.body.admin_id
        let info = '';
        if (employee && admin) {
            if (req.body.info != undefined) {
                info = req.body.info;
            }
            let sql = 'SELECT status FROM items WHERE id = "' + itemsID + '"'
            db.query(sql, (err, result) => {
                if (err) {
                    res.status(500).json({
                        message: {
                            errno: err.errno,
                            errdecode: err.code,
                            data: err
                        }
                    });
                } else {
                    if (result[0].status == 'Available') {
                        sql = 'UPDATE items SET status = "Borrowed" WHERE id = ' + itemsID + ''
                        db.query(sql, (err, results) => {
                            if (err) {
                                res.status(500).json({
                                    message: {
                                        errno: err.errno,
                                        errdecode: err.code,
                                        data: err
                                    }
                                });
                            } else {

                                sql = 'INSERT INTO history (id_item, id_employee, admin_approve, timestamp, status, info) '
                                sql += 'VALUES (' + itemsID + ',' + employee + ', ' + admin + ', CURRENT_TIMESTAMP, "Borrow", "' + info + '")'
                                db.query(sql, (err, finished) => {
                                    if (err) {
                                        res.status(500).json({
                                            message: {
                                                errno: err.errno,
                                                errdecode: err.code,
                                                data: err
                                            }
                                        });
                                    } else {
                                        res.status(201).json({
                                            message: 'CREATED',
                                        });
                                    }
                                });
                            }
                        })
                    } else {
                        res.status(400).send({
                            message: 'Bad request',
                        });
                    }
                }
            })
        } else {
            res.status(400).send({
                message: 'Bad request',
                detial: "Server can't found employee_id or admin_id"
            });
        }
    }

    returns(req, res) {
        let itemsID = req.params.id;
        let admin = req.body.admin_id
        if (admin) {
            let sql = 'SELECT id, id_employee FROM history WHERE id_item = ' + itemsID + ' ORDER BY timestamp DESC LIMIT 1'
            let info = '';
            if (req.body.info != undefined) {
                info = req.body.info;
            }
            db.query(sql, (error, data) => {
                if (error) {
                    res.status(500).json({
                        message: {
                            errno: error.errno,
                            errdecode: error.code,
                            data: error
                        }
                    });
                } else {
                    let sql = 'SELECT status FROM items WHERE id = "' + itemsID + '"'
                    db.query(sql, (err, result) => {
                        if (err) {
                            res.status(500).json({
                                message: {
                                    errno: err.errno,
                                    errdecode: err.code,
                                    data: err
                                }
                            });
                        } else {
                            if (result[0].status == 'Borrowed') {
                                sql = 'UPDATE items SET status = "Available" WHERE id = ' + itemsID + ''
                                db.query(sql, (err, results) => {
                                    if (err) {
                                        res.status(500).json({
                                            message: {
                                                errno: err.errno,
                                                errdecode: err.code,
                                                data: err
                                            }
                                        });
                                    } else {

                                        sql = 'INSERT INTO history (id_item, admin_approve, timestamp, status, info, id_employee) '
                                        sql += 'VALUES (' + itemsID + ', ' + admin + ', CURRENT_TIMESTAMP, "Return", "' + info + '", ' + data[0].id_employee + ')'
                                        db.query(sql, (err, finished) => {
                                            if (err) {
                                                res.status(500).json({
                                                    message: {
                                                        errno: err.errno,
                                                        errdecode: err.code,
                                                        data: err
                                                    }
                                                });
                                            } else {
                                                res.status(201).json({
                                                    message: 'CREATED',
                                                });
                                            }
                                        });
                                    }
                                })
                            } else {
                                res.status(400).send({
                                    message: 'Bad request',
                                });
                            }
                        }
                    })
                }
            })
        } else {
            res.status(400).send({
                message: 'Bad request',
            });
        }

    }

    brands(req, res) {
        let sql = 'SELECT * FROM brand';
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                res.status(200).send({
                    data: {
                        brands: result
                    }
                })
            }
        })
    }

    types(req, res) {
        let sql = 'SELECT * FROM type';
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                res.status(200).send({
                    data: {
                        types: result
                    }
                })
            }
        })
    }

    os(req, res) {
        let sql = 'SELECT * FROM os';
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                res.status(200).send({
                    data: {
                        os: result
                    }
                })
            }
        })
    }

    owners(req, res) {
        let sql = 'SELECT * FROM owner';
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                res.status(200).send({
                    data: {
                        owners: result
                    }
                })
            }
        })
    }

    showEmployee(req, res) {
        let sql = 'SELECT * FROM employee';
        let query = db.query(sql, (err, result) => {
            if (err) throw err;
            else {
                res.status(200).send({
                    data: {
                        employee: result
                    }
                })
            }
        })
    }

    history(req, res) {
        let id = req.query.id
        if (id) {
            if (validator.isNumeric(id)) {
                let sql = 'SELECT a.id, a.id_item, b.name as itemName, a.id_employee, c.name as employeeName, a.admin_approve, d.name as adminName, UNIX_TIMESTAMP(a.timestamp) AS time, a.status, a.info '
                sql += 'FROM history as a, items as b, employee as c, admin as d '
                sql += 'WHERE a.id_item = b.id AND a.id_employee = c.id AND a.admin_approve = d.id AND a.id_item =' + id + ' AND a.status != "Delete" AND a.status != "Edit" AND a.status != "Add" ORDER BY a.timestamp DESC '
                let query = db.query(sql, (err, result) => {
                    if (err) {
                        res.status(500).json({
                            message: {
                                errno: err.errno,
                                errdecode: err.code,
                                data: err
                            }
                        });
                    }
                    else {
                        if (result.length < 1) {
                            res.status(404).send({
                                message: 'Not Found'
                            })
                        } else {
                            res.status(200).send({
                                data: {
                                    history: result
                                }
                            })
                        }
                    }
                })
            } else {
                res.status(400).send({
                    message: 'Bad request',
                });
            }
        } else {
            let sql = 'SELECT a.id, a.id_item, b.name as itemName, a.id_employee, c.name as employeeName, a.admin_approve, d.name as adminName, UNIX_TIMESTAMP(a.timestamp) AS time, a.status, a.info '
            sql += 'FROM history as a, items as b, employee as c, admin as d '
            sql += 'WHERE a.id_item = b.id AND a.id_employee = c.id AND a.admin_approve = d.id ORDER BY a.timestamp DESC '
            let query = db.query(sql, (err, result) => {
                if (err) {
                    res.status(500).json({
                        message: {
                            errno: err.errno,
                            errdecode: err.code,
                            data: err
                        }
                    });
                }
                else {
                    res.status(200).send({
                        data: {
                            history: result
                        }
                    })
                }
            })
        }
    }

}


module.exports = Api;