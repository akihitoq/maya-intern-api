const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const mysql = require('mysql');
const apiFunction = require('./api');
const oauthserver = require('oauth2-server');
const memorystore = require('./model.js');
var redirectToHTTPS = require('express-http-to-https').redirectToHTTPS
let middleware = require('./middleware');

var crypto = require('crypto');

const multer = require('multer');

const db = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE
});

db.connect();



// Use in localhost

const certificate = fs.readFileSync('./cert/localhost.crt', 'utf8');
const privateKey = fs.readFileSync('./cert/localhost.key', 'utf8');

// Use in server
// const privateKey = fs.readFileSync('/etc/letsencrypt/live/boss.in.th/privkey.pem', 'utf8');
// const certificate = fs.readFileSync('/etc/letsencrypt/live/boss.in.th/cert.pem', 'utf8');
// const ca = fs.readFileSync('/etc/letsencrypt/live/boss.in.th/chain.pem', 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    // ca: ca
};


function main() {
    let app = express();
    let api = new apiFunction();
    const port = process.env.PORT || 80;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(redirectToHTTPS([/localhost:(\d{4})/], [/\/insecure/], 301));
    const httpsServer = https.createServer(credentials, app);

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.oauth = oauthserver({
        model: memorystore,
        grants: ['password', 'refresh_token'],
        debug: true,
        accessTokenLifetime: memorystore.JWT_ACCESS_TOKEN_EXPIRY_SECONDS,   // expiry time in seconds, consistent with JWT setting in model.js
        refreshTokenLifetime: memorystore.JWT_REFRESH_TOKEN_EXPIRY_SECONDS   // expiry time in seconds, consistent with JWT setting in model.js
      });


    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './uploads/items');
        },
        filename: function (req, file, cb) {
            var customFileName = crypto.randomBytes(18).toString('hex'),
                fileExtension = file.originalname.split('.')[1]
            cb(null, 'item-' + Date.now() + '.' + fileExtension);
        }
    });

    var upload = multer({ storage: storage });

    app.use('/uploads', express.static('./uploads'))



    app.use(bodyParser.json());
    app.all('/oauth/token', app.oauth.grant());
    app.all('/api/*', app.oauth.authorise());
    app.use(app.oauth.errorHandler());

    app.get('/api/items', api.listItems);

    var uploading = upload.single('image')

    app.post('/api/upload', uploading, (req, res) => {
        if (!req.file) {
            res.status(500).json({
                message: 'Upload fail'
            });
        } else {
            res.status(200).send({
                image: 'https://localhost/uploads/items/' + req.file.filename,
                timestamp: Date.now()
            })
        }
    })

    app.post('/api/items', api.addItem);
    app.get('/api/items/filter', api.filterPlatform);
    app.put('/api/items/:id', api.editItem);
    app.delete('/api/items/:itemID', api.deleteItem);
    app.post('/api/register/admins');
    app.get('/api/items/:item', api.getItemDetial);
    app.get('/api/employee', api.showEmployee);
    app.get('/api/brands', api.brands);
    app.get('/api/types', api.types);
    app.get('/api/os', api.os);
    app.get('/api/owners', api.owners);
    app.get('/api/history', api.history);

    app.post('/api/items/borrow/:id', api.borrows);
    app.post('/api/items/return/:id', api.returns);

    app.get('*', (req, res) => {
        res.status(404).send({
            status: 404,
            detial: 'Not found'
        });

    });
    http.createServer(app).listen(port, () => {
        console.log('HTTP Server running on port 80');
    });

    httpsServer.listen(443, () => {
        console.log('HTTPS Server running on port 443');
    });
}

main();